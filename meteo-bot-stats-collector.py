import logging
import os

from utils import Database, ClimateStation


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


def main():
    logger.info('Starting to collect statistics...')

    database = Database(os.path.join(os.path.dirname(__file__), '..', 'climate_stats.db'))
    climate_station = ClimateStation()

    stats = climate_station.sample()

    database.insert_stats(stats)

    logger.info(stats)
    logger.info('Statistics collection has been finished')


if __name__ == '__main__':
    main()
