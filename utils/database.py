import logging
import sqlite3


class Database:
    logger = logging.getLogger(__name__)

    class Decorators:
        @staticmethod
        def log(function):
            def wrapper(self, *args, **kwargs):
                try:
                    return function(self, *args, **kwargs)
                except sqlite3.DatabaseError:
                    self.logger.error(f'DatabaseError executing {function.__name__}{args} {kwargs}', exc_info=True)

            return wrapper

    def __init__(self, database_file_path):
        self.__connection = sqlite3.connect(database_file_path)
        self.__connection.row_factory = self.__dict_factory

    @staticmethod
    def __dict_factory(cursor, row):
        d = dict()
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    @Decorators.log
    def close_connection(self):
        self.__connection().close()

    @Decorators.log
    def insert_stats(self, stats):
        self.__connection.cursor().execute(
            'INSERT INTO stats (timestamp, temperature, pressure, humidity) VALUES (?, ?, ?, ?)',
            (stats.timestamp, stats.temperature, stats.pressure, stats.humidity))
        self.__connection.commit()

